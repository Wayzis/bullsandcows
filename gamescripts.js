var quest;
var cow;
var bull;
var hist;
var turn;
var lim;
var active;
var b1p;
var b2p;
var b3p;
var b4p;
//Загрузка картинок и инициализация переменных
function Init() {
	Start();
	hist = [];
	active = true;
	lim = false;
	b1p = new Image();
	b2p = new Image();
	b3p = new Image();
	b4p = new Image();
	b1p.src = "bull1.png";
	b2p.src = "bull2.png";
	b3p.src = "bull3.png";
	b4p.src = "bull4.png";
}
function StartNew() {
	Start();
}
function KeyCatch(e) {
	//Если ловим Enter, обрабатываем иначе
	if (e.keyCode == 13) {
		event.preventDefault();
		//Делаем ход
		Step();
		//Меняем надписи
		document.getElementById("h").textContent = "Сделано ходов: " + turn;
		document.getElementById("input").value = "";
		return false;
	}
}
function Start(q) {
	//Очищаем историю
	UpdateHistory();
	active = true;
	//Подготавливаем интерфейс
	lim = document.getElementById("limit").checked;
	document.getElementById("h").textContent = "";
	document.getElementById("canv").hidden = true;
	turn = 0;
	//Если число не задано, генерируем случайное
	if (!q) {
		quest = "";
		pool = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
		//Пока число не будет четырёхзначным
		while (quest.length < 4) {
			let el = Math.floor(Math.random() * pool.length);
			quest += pool.splice(el, 1)[0];
		}
	}
	else
		quest = String(q);
}
function Step() {
	let inp = document.getElementById("input").value;
	if (!inp || !active)
		return;
	//Если число отгадано
	if (quest == inp) {
		UpdateHistory(inp, 4, 0);
		active = false;
		alert("Ты выиграл!");
		//Рисуем быков
		canva = document.getElementById("canv");
		canva.hidden = false;
		cont = canva.getContext("2d");
		cont.drawImage(b1p, 0, 0, 300, 300);
		cont.drawImage(b2p, 100, 0, 300, 300);
		cont.drawImage(b3p, 200, 0, 300, 300);
		cont.drawImage(b4p, 300, 0, 300, 300);
		return;
	}
	//Подсчитываем быков и коров для каждой позиции
	bull = 0;
	cow = 0;
	for (let i = 0; i < 4; i++) {
		if (quest[i] == inp[i])
			bull++;
		else if (quest.includes(inp[i]))
			cow++;
	}
	//Добавляем в историю
	UpdateHistory(inp, bull, cow);
	turn++;
	//Если есть ограничение и ходы кончились
	if (lim & (turn == 10))
	{
		alert("Попытки кончились!");
		active = false;
	}
}
function UpdateHistory(a, b, c) {
	let text = document.getElementById("history");
	//Очищаем историю, если нужно
	if (!a || a == "") {
		hist = [];
		text.value = "";
		return;
	}
	let len = hist.length;
	let out = "";
	//Добавляем запись о сделанном ходе
	if (b == 4)
		hist[len++] = a + " ( число отгадано! )";
	else
		hist[len++] = a + " (" + b + " быков и " + c + " коров)";
	if (lim & (hist.length == 10))
		hist[len++] = "==== игра окончена ====";
	//Собираем все записи и пишем на страницу
	for (let i = 0; i < len; i++) {
		out += hist[i];
		if (i != len)
			out += "\r\n";
	}
	text.value = out;
	//Возвращаем фокус на поле ввода
	document.getElementById("input").focus();
}